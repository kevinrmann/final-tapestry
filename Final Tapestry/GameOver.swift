//
//  GameOver.swift
//  Final Tapestry
//
//  Created by Kevin Mann on 11/29/14.
//  Copyright (c) 2014 Freedom Software. All rights reserved.
//

import Foundation


class GameOver : GameState {
    override init() {
        super.init()
        self.name = "game over"
        self.description = "Game Over!"
    }
}