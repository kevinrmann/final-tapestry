//
//  Weapon.swift
//  Final Tapestry
//
//  Created by Kevin Mann on 11/28/14.
//  Copyright (c) 2014 Freedom Software. All rights reserved.
//

import Foundation


public class Weapon {
    
    var name : String
    var healthDamage : Int
    var staminaDamage : Int
    var manaDamage : Int

    init () {
        self.name = "Fists"
        self.healthDamage = -1
        self.staminaDamage = -1
        self.manaDamage = -1
    }
    
}