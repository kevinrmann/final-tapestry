//
//  GameStateController.swift
//  Final Tapestry
//
//  Created by Kevin Mann on 11/24/14.
//  Copyright (c) 2014 Freedom Software. All rights reserved.
//

import Foundation

class GameStateManager {
    class var sharedInstance : GameStateManager {
        struct Static {
            static let instance : GameStateManager = GameStateManager()
        }
        return Static.instance
    }
    
    var currentState : GameState
    var currentCharacter : GameCharacter
    var report: String
    
    init() {
        self.currentState = HomeState()
        self.currentCharacter = Human(name: "Kevin")
        self.report = ""
    }
    
    func getCurrentState() -> GameState {
        return self.currentState
    }
    
    func getReport() -> String {
        var report = self.report
        self.report = ""
        return report
    }
    
    func getEnemyReport() -> String {
        var enemyReport = ""
        for enemy in self.currentState.enemies {
            enemyReport = "\(enemy.name): \(enemy.currentHealth)\n\(enemyReport)"
        }
        return enemyReport
    }
    
    func getMainStateDescription() -> String {
        return self.currentState.description
    }
    
    func setCurrentState(newState: GameState) {
        self.currentState = newState
    }
    
    func setCurrentCharacter(newCharacter: GameCharacter) {
        self.currentCharacter = newCharacter
    }
    
    func getCurrentCharacter() -> GameCharacter {
        return self.currentCharacter
    }
    
    func getAvailableCommands() -> String {
        return self.currentState.getAvailableCommands()
    }
    
    func getStats() -> String {
        return self.currentCharacter.getCurrentStats()
    }
    
    func validateCommand(command: String) -> Bool {
        return self.currentState.validateCommand(command)
    }
    
    func isGameOver() -> Bool {
        if (self.currentCharacter.currentHealth <= 0) {
            return true
        }
        return false
    }
    
    func isEnded() -> Bool {
        return self.currentState.isEnded(self.currentCharacter)
    }
    
    func queueCommand(command: String) {
        if (self.currentState.name == "battle" && command == "attack") {
            var enemies = self.currentState.enemies
            for enemy in enemies {
                if (enemy.currentHealth > 0) {
                    enemy.queueAttack(Attack(reciever: enemy, name: "attack", weapon: self.currentCharacter.currentWeapon))
                }
            }
        }
        if (self.currentState.name == "battle" && command == "end") {
            for enemy in self.currentState.enemies {
                enemy.executeNextAttack()
            }
            self.report = self.currentState.cleanEnemies()
            for enemy in self.currentState.enemies {
                self.currentCharacter.queueAttack(Attack(reciever: self.currentCharacter, name: "enemy attack", weapon: enemy.getWeapon()))
            }
            for enemy in self.currentState.enemies {
                self.currentCharacter.executeNextAttack()
            }
            if (self.currentCharacter.currentHealth <= 0) {
                self.report = "\(self.report)Unfortunately the enemies killed you..."
            } else {
                if (self.currentState.enemies.count <= 0) {
                    self.report = "\(self.report)You gained: \(self.currentState.experienceEarned) experience"
                    self.currentCharacter.addExperience(exp: self.currentState.experienceEarned)
                }
            }
        }
    }
}