//
//  GameViewController.swift
//  Final Tapestry
//
//  Created by Kevin Mann on 11/24/14.
//  Copyright (c) 2014 Freedom Software. All rights reserved.
//

import Foundation

import UIKit

class GameViewController: UIViewController, UITextFieldDelegate {
    
    let gameStateManager = GameStateManager.sharedInstance
    let commandLibrary = CommandLibrary()
    
    @IBOutlet weak var mainStatusLabel: UILabel!
    @IBOutlet weak var commandTextField: UITextField!
    @IBOutlet weak var commandsLabel: UILabel!
    @IBOutlet weak var commandResponseLabel: UILabel!
    @IBOutlet weak var incorrectImage: UIImageView!
    @IBOutlet weak var correctImage: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mainStatusLabel.text = "Welcome, \(gameStateManager.getCurrentCharacter().name)!\nThe Quest Begins!"
        commandTextField.delegate = self
        commandsLabel.text = ""
        commandResponseLabel.numberOfLines = 0
        commandResponseLabel.sizeToFit()
        commandResponseLabel.text = "One day you wake up in your cottage cheese cottage and you decide... \"Time for a quest!\""
        gameStateManager.setCurrentState(HomeState())
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool // called when 'return' key
    {
        textField.resignFirstResponder()
        return true;
    }
    
    func showCommandStatus(newStatus: Bool) {
        if (newStatus) {
            self.incorrectImage.hidden = true
            self.correctImage.hidden = false
        } else {
            self.incorrectImage.hidden = false
            self.correctImage.hidden = true
        }
    }
    
    @IBAction func enteredCommand(sender: AnyObject) {
        commandsLabel.hidden = false
        let commandText : String? = commandTextField.text!
        if (commandText != nil) {
            // Add the most recent command to the command history label
            commandsLabel.text = "\(commandText!)\n\(commandsLabel.text!)"
            if (self.gameStateManager.validateCommand(commandText!)) {
                self.showCommandStatus(true)
                // Put the command on the queue
                self.gameStateManager.queueCommand(commandText!)
                // Get the response for the command
                self.commandResponseLabel.text = commandLibrary.commandResponseLookup(commandText!)
                let newState = commandLibrary.newStateLookup(commandText!)
                self.gameStateManager.setCurrentState(newState)
            }
            else {
                self.showCommandStatus(false)
                var commandResponse = self.gameStateManager.getAvailableCommands()
                self.commandResponseLabel.text = "Sorry, invalid command, you can only do these commands right now:\n\n\(commandResponse)"
            }
        }
        self.mainStatusLabel.text = self.gameStateManager.getMainStateDescription()
    }
}