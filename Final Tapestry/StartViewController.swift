//
//  ViewController.swift
//  Final Tapestry
//
//  Created by Kevin Mann on 11/24/14.
//  Copyright (c) 2014 Freedom Software. All rights reserved.
//

import UIKit

class StartViewController: UIViewController, UITextFieldDelegate {
    
    let gameStateManager = GameStateManager.sharedInstance
    @IBOutlet weak var nameTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameTextField.delegate = self
        gameStateManager.setCurrentState(HomeState())
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textFieldShouldReturn(textField: UITextField!) -> Bool // called when 'return' key
    {
        textField.resignFirstResponder()
        return true;
    }

    @IBAction func choseDwarf(sender: AnyObject) {
        let playerName : String? = nameTextField.text!
        if (playerName != nil) {
            let newCharacter = Dwarf(name: playerName!)
            self.gameStateManager.setCurrentCharacter(newCharacter)
        }
    }
    
    @IBAction func choseElf(sender: AnyObject) {
        let playerName : String? = nameTextField.text!
        if ((playerName) != nil) {
            let newCharacter = Elf(name: playerName!)
            self.gameStateManager.setCurrentCharacter(newCharacter)
        }
    }
    
    @IBAction func choseHuman(sender: AnyObject) {
        let playerName : String? = nameTextField.text!
        if ((playerName) != nil) {
            let newCharacter = Human(name: playerName!)
            self.gameStateManager.setCurrentCharacter(newCharacter)
        }
    }
    
}

