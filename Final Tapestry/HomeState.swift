//
//  PrepState.swift
//  Final Tapestry
//
//  Created by Kevin Mann on 11/25/14.
//  Copyright (c) 2014 Freedom Software. All rights reserved.
//

import Foundation

class HomeState : GameState {
    override init() {
        super.init()
        self.name = "home"
        self.description = "At Home"
        // Append commands needed for home state
        self.availableCommands.append("leave")
    }
}