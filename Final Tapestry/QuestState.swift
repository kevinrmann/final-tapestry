//
//  QuestState.swift
//  Final Tapestry
//
//  Created by Kevin Mann on 11/27/14.
//  Copyright (c) 2014 Freedom Software. All rights reserved.
//

import Foundation

class QuestState : GameState {
    override init() {
        super.init()
        self.name = "quest"
        self.description = "On a Quest!"
        // Append commands needed for home state
        self.availableCommands.append("battle")
    }
}