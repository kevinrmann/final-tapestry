//
//  Staff.swift
//  Final Tapestry
//
//  Created by Kevin Mann on 11/28/14.
//  Copyright (c) 2014 Freedom Software. All rights reserved.
//

import Foundation


class Staff: Weapon {
    override init() {
        super.init()
        self.name = "Staff"
        self.healthDamage = -10
        self.staminaDamage = -10
        self.manaDamage = -10
    }
}