//
//  Sword.swift
//  Final Tapestry
//
//  Created by Kevin Mann on 11/29/14.
//  Copyright (c) 2014 Freedom Software. All rights reserved.
//

import Foundation


class Sword: Weapon {
    override init() {
        super.init()
        self.name = "Sword"
        self.healthDamage = -15
        self.staminaDamage = -15
        self.manaDamage = -15
    }
}