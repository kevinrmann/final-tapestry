//
//  Attack.swift
//  Final Tapestry
//
//  Created by Kevin Mann on 11/28/14.
//  Copyright (c) 2014 Freedom Software. All rights reserved.
//

import Foundation


class Attack {
    var attackReciever : GameCharacter
    var name : String
    var weapon : Weapon
    
    init (reciever: GameCharacter, name: String, weapon: Weapon) {
        self.attackReciever = reciever
        self.name = name
        self.weapon = weapon
    }
    
    func executeAttack() {
        var changeInhealth = self.weapon.healthDamage
        var changeInMana = self.weapon.manaDamage
        var changeInStamina = self.weapon.staminaDamage
        self.attackReciever.recieveAttack(healthDamage: changeInhealth, manaDamage: changeInMana, staminaDamage: changeInStamina)
    }
}