//
//  Dwarf.swift
//  Final Tapestry
//
//  Created by Kevin Mann on 11/24/14.
//  Copyright (c) 2014 Freedom Software. All rights reserved.
//

import Foundation

class Dwarf : GameCharacter {
    
    override init(name:String) {
        super.init(name: name)
        self.type = "dwarf"
        self.currentHealth = 140
        self.maxHealth = 140
        self.currentStamina = 80
        self.maxStamina = 80
        self.currentMana = 30
        self.maxMana = 30
    }
}