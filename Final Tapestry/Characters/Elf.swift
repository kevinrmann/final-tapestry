//
//  Elf.swift
//  Final Tapestry
//
//  Created by Kevin Mann on 11/24/14.
//  Copyright (c) 2014 Freedom Software. All rights reserved.
//

import Foundation

class Elf : GameCharacter {
    
    override init(name:String) {
        super.init(name: name)
        self.type = "elf"
        self.currentHealth = 120
        self.maxHealth = 120
        self.currentStamina = 80
        self.maxStamina = 80
        self.currentMana = 60
        self.maxMana = 60
    }
}