//
//  Human.swift
//  Final Tapestry
//
//  Created by Kevin Mann on 11/24/14.
//  Copyright (c) 2014 Freedom Software. All rights reserved.
//

import Foundation

class Human : GameCharacter {
    
    override init(name:String) {
        super.init(name: name)
        self.type = "human"
        self.currentHealth = 100
        self.maxHealth = 100
        self.currentStamina = 90
        self.maxStamina = 90
        self.currentMana = 50
        self.maxMana = 50
    }
}