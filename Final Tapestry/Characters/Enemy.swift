//
//  Enemy.swift
//  Final Tapestry
//
//  Created by Kevin Mann on 11/28/14.
//  Copyright (c) 2014 Freedom Software. All rights reserved.
//

import Foundation


class Enemy : GameCharacter {
    override init(name:String) {
        super.init(name: name)
        self.type = "enemy"
        self.currentHealth = 50
        self.maxHealth = 50
        self.currentStamina = 50
        self.maxStamina = 50
        self.currentMana = 30
        self.maxMana = 30
        self.currentWeapon = Dagger()
    }
    
    convenience init(name: String, weapon: Weapon) {
        self.init(name: name)
        self.type = "enemy"
        self.currentHealth = 50 + Int(arc4random_uniform(40) + 1)
        self.maxHealth = 50
        self.currentStamina = 50
        self.maxStamina = 50
        self.currentMana = 30
        self.maxMana = 30
        self.currentWeapon = weapon
    }
    
    // Static factory method for random enemies
    class func GetRandomEnemy() -> Enemy {
        func getRandomWeapon(weaponNumber: Int) -> Weapon {
            switch weaponNumber {
            case 1:
                return Weapon()
            case 2:
                return Staff()
            case 3:
                return Dagger()
            case 4:
                return Sword()
            default:
                return Weapon()
            }
        }
        func getRandomName(nameNumber: Int) -> String {
            switch nameNumber {
            case 1:
                return "Grunt"
            case 2:
                return "Golum"
            case 3:
                return "Hornet"
            case 4:
                return "Charizard"
            case 5:
                return "Hitler"
            case 6:
                return "Zombie"
            case 7:
                return "Vampire"
            case 8:
                return "Orc"
            case 9:
                return "Fart"
            case 10:
                return "Zebra"
            default:
                return "Leech"
            }
        }
        var randomWeapon = getRandomWeapon(Int(arc4random_uniform(4) + 1))
        var randomName : String = getRandomName(Int(arc4random_uniform(10) + 1))
        return Enemy(name: randomName, weapon: randomWeapon)
    }

}