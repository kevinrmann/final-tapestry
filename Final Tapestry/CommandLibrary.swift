//
//  CommandLibrary.swift
//  Final Tapestry
//
//  Created by Kevin Mann on 11/27/14.
//  Copyright (c) 2014 Freedom Software. All rights reserved.
//

import Foundation


class CommandLibrary {
    
    var gameStateManager = GameStateManager.sharedInstance
    
    func commandResponseLookup(command: String) -> String {
        switch command {
        case "help":
            return "Right now you can do these things:\n\n\(gameStateManager.getAvailableCommands())"
        case "stats":
            return "\(gameStateManager.getCurrentCharacter().name) has:\n\n\(gameStateManager.getStats())"
        case "leave":
            return "You leave your cottage and now you are on your quest! What will you do next?"
        case "battle":
            return "You found a battle!"
        case "end":
            return "Ending turn and executing an attack.\n\n\(gameStateManager.getReport())"
        case "attack":
            return "Queued an attack for each enemy"
        case "run":
            return "You escaped from the battle!"
        case "list":
            return "Your enemies:\n\n\(gameStateManager.getEnemyReport())"
        default:
            return "Valid command but we still have to implement that command"
        }
    }
    
    func newStateLookup(command: String) -> GameState {
        switch command {
        case "help":
            return gameStateManager.getCurrentState()
        case "stats":
            return gameStateManager.getCurrentState()
        case "leave":
            return QuestState()
        case "home":
            return HomeState()
        case "battle":
            return BattleState.GetBattle(gameStateManager.currentState, currentPlayer: gameStateManager.currentCharacter)
        case "attack":
            return BattleState.GetBattle(gameStateManager.currentState, currentPlayer: gameStateManager.currentCharacter)
        case "end":
            if (gameStateManager.isEnded()) {
                if (gameStateManager.isGameOver()) {
                    return GameOver()
                }
                return QuestState()
            } else {
                return BattleState.GetBattle(gameStateManager.currentState, currentPlayer: gameStateManager.currentCharacter)
            }
        case "list":
            return gameStateManager.getCurrentState()
        case "run":
            return QuestState()
        default:
            return QuestState()
        }
    }
}