//
//  BattleState.swift
//  Final Tapestry
//
//  Created by Kevin Mann on 11/25/14.
//  Copyright (c) 2014 Freedom Software. All rights reserved.
//

import Foundation


class BattleState : GameState {
    
    override init() {
        super.init()
        self.name = "battle"
        self.description = "In a Battle"

        // Append commands needed for battle state
        self.availableCommands.append("run")
        self.availableCommands.append("attack")
        self.availableCommands.append("list")
        self.availableCommands.append("end")
    }
    
    convenience init(numberOfEnemies: Int, currentPlayer: GameCharacter) {
        self.init()
        for var i = 0; i < numberOfEnemies; i++ {
            self.enemies.append(Enemy.GetRandomEnemy())
        }
        self.description = "In a Battle: \(numberOfEnemies) enemies"
    }
    
    override func isEnded(player: GameCharacter) -> Bool {
        if (enemies.count == 0 || player.currentHealth <= 0) {
            return true
        }
        return false
    }
    
    override func cleanEnemies() -> String {
        var report = ""
        var enemiesRemoved = 0
        for (index, enemy) in enumerate(self.enemies) {
            if (enemy.currentHealth <= 0) {
                experienceEarned += enemy.maxHealth
                report = "\(report)You killed:\t\(enemy.name)\n"
                self.enemies.removeAtIndex(index - enemiesRemoved)
                enemiesRemoved++
            }
        }
        return report
    }
    
    // Static factory method for battles. This allows us to have the same
    // battle persist until the battle has ended. It also allows us to generate 
    // random battles with different amounts of enemies.
    class func GetBattle(currentState: GameState, currentPlayer: GameCharacter) -> GameState {
        if (currentState.isEnded(currentPlayer)) {
            var enemies : Int = Int(arc4random_uniform(5) + 1)
            return BattleState(numberOfEnemies: enemies, currentPlayer: currentPlayer)
        }
        return currentState
    }
}