//
//  GameCharacter.swift
//  Final Tapestry
//
//  Created by Kevin Mann on 11/24/14.
//  Copyright (c) 2014 Freedom Software. All rights reserved.
//

import Foundation


class GameCharacter {

    var name : String
    var type : String
    
    var currentHealth : Int
    var maxHealth : Int
    var currentStamina : Int
    var maxStamina : Int
    var currentMana : Int
    var maxMana : Int
    
    var currentMoney : Int
    var currentLevel : Int
    var currentExperience : Int
    var currentWeapon : Weapon
    
    var attacksWaiting : [Attack]
    
    init(name:String) {
        if (name == "") {
            self.name = "Kevin"
        } else {
            self.name = name
        }
        self.type = "human"
        
        self.currentHealth = 100
        self.maxHealth = 100
        self.currentStamina = 100
        self.maxStamina = 100
        self.currentMana = 100
        self.maxMana = 100
        
        self.currentMoney = 50
        self.currentLevel = 1
        self.currentExperience = 100
        self.currentWeapon = Staff()
        
        self.attacksWaiting = []
    }
    
    func setWeapon(newWeapon: Weapon) {
        self.currentWeapon = newWeapon
    }
    
    func getWeapon() -> Weapon {
        return self.currentWeapon
    }
    
    func queueAttack(attack: Attack) {
        self.attacksWaiting.append(attack)
    }
    
    func addExperience(#exp: Int) {
        self.currentExperience += exp
        var prevLevel = self.currentLevel
        self.currentLevel = self.currentExperience / 100
        if (self.currentLevel > prevLevel) {
            self.maxHealth += self.currentLevel * 10
            self.maxMana += self.currentLevel * 5
            self.maxStamina += self.currentLevel * 5
            self.currentHealth = self.maxHealth
            self.currentMana = self.maxMana
            self.currentStamina = self.maxStamina
        }
    }
    
    func executeNextAttack() {
        if (self.attacksWaiting.count > 0) {
            self.attacksWaiting[0].executeAttack()
            self.attacksWaiting.removeAtIndex(0)
        }
    }
    
    func recieveAttack(#healthDamage: Int, manaDamage: Int, staminaDamage: Int) {
        self.currentHealth += healthDamage
        self.currentMana += manaDamage
        self.currentStamina += staminaDamage
        
        if (self.currentHealth < 0) {
            self.currentHealth = 0
        }
        if (self.currentMana < 0) {
            self.currentMana = 0
        }
        if (self.currentStamina < 0) {
            self.currentStamina = 0
        }
    }
    
    func getCurrentStats() -> String {
        var statString = "Health:\t\(currentHealth)\nMana:\t\(currentMana)\nStamina:\t\(currentStamina)\nWeapon:\t\(currentWeapon.name)\nLevel:\t\(currentLevel)\nExperience:\t\(currentExperience)"
        return statString
    }
}