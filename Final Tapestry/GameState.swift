//
//  GameState.swift
//  Final Tapestry
//
//  Created by Kevin Mann on 11/25/14.
//  Copyright (c) 2014 Freedom Software. All rights reserved.
//

import Foundation


class GameState {
    var name : String
    var description : String
    var availableCommands : [String]
    var enemies : [Enemy] = []
    var experienceEarned = 0
    
    init() {
        self.name = "base"
        self.description = "base state"
        self.availableCommands = ["stats", "help"]
    }
    
    func getAvailableCommands() -> String {
        var commandsString = ""
        for c in availableCommands {
            commandsString += "\(c)\n"
        }
        return commandsString
    }
    
    func validateCommand(command: String) -> Bool {
        for c in availableCommands {
            if (command == c) {
                return true
            }
        }
        return false
    }
    
    func isEnded(player: GameCharacter) -> Bool {
        return true
    }
    
    func cleanEnemies() -> String {
        return ""
    }
    
    func getCommandResponse(command: String) -> String {
        return "Successful command"
    }
    
    func queueCommandAndGetNewState(command: String) -> GameState {
        return self
    }
}