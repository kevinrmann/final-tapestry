//
//  Dagger.swift
//  Final Tapestry
//
//  Created by Kevin Mann on 11/28/14.
//  Copyright (c) 2014 Freedom Software. All rights reserved.
//

import Foundation


class Dagger: Weapon {
    override init() {
        super.init()
        self.name = "Staff"
        self.healthDamage = -5
        self.staminaDamage = -5
        self.manaDamage = -5
    }
}