// Remove the first element of an array

import Foundation

var array = ["attack1", "attack2", "attack3"]
array[0]
array.removeAtIndex(0)
array



// Remove an element while iterating (use enumerate)
var list = ["enemy1", "enemy2", "enemy3"]
var numberRemoved = 0

for (index, item) in enumerate(list) {
    println(index)
    if (item == "enemy2" || item == "enemy1" || item == "enemy3") {
        list.removeAtIndex(index - numberRemoved)
        numberRemoved++
    }
}

println(list)


list = ["enemy1", "enemy2", "enemy3"]

for i in 1...list.count {
    println(i)
}

// Static factory method for random enemies

func getRandomName(nameNumber: Int) -> String {
    switch nameNumber {
    case 1:
        return "Grunt"
    case 2:
        return "Golum"
    case 3:
        return "Hornet"
    case 4:
        return "Charizard"
    case 5:
        return "Hitler"
    case 6:
        return "Zombie"
    case 7:
        return "Vampire"
    case 8:
        return "Orc"
    case 9:
        return "Fart"
    case 10:
        return "Zebra"
    default:
        return "Leech"
    }
}

var randomName = getRandomName(Int(arc4random_uniform(10) + 1))

println(randomName)

randomName = getRandomName(Int(arc4random_uniform(10) + 1))

println(randomName)


150 / 100






