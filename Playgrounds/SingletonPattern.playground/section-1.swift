// Singletons in swift

import UIKit

class GameStateManager {
    class var sharedInstance : GameStateManager {
        struct Static {
            static let instance : GameStateManager = GameStateManager()
        }
        return Static.instance
    }
    
    var currentState : String
    
    init() {
        self.currentState = "wait"
    }
    
    func getCurrentState() -> String {
        return self.currentState
    }
    
    func setCurrentState(newState:String) {
        self.currentState = newState
    }
    
}


var instanceOne = GameStateManager.sharedInstance

var instanceTwo = GameStateManager.sharedInstance

instanceOne.setCurrentState("Butt")

instanceTwo.getCurrentState()

instanceOne.getCurrentState()

