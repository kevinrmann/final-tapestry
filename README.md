# Final Tapestry #

This Kevin Mann's final project for CS 5700 taught by Chris Thatcher at USU. It is an iOS app (written in Swift) that uses 4 different design patterns: Singleton, Command, Composite, and Strategy.

### How does the game work? ###

At the opening screen users can choose their character name and their character's race. The various races have different starting stats. ![iOS Simulator Screen Shot Dec 2, 2014, 7.10.18 PM.png](https://bitbucket.org/repo/Apbnby/images/2430513531-iOS%20Simulator%20Screen%20Shot%20Dec%202,%202014,%207.10.18%20PM.png)

After the user chooses a race they are brought to the game. ![iOS Simulator Screen Shot Dec 2, 2014, 7.15.00 PM.png](https://bitbucket.org/repo/Apbnby/images/3694211203-iOS%20Simulator%20Screen%20Shot%20Dec%202,%202014,%207.15.00%20PM.png)

Here they are presented with a main header which will always display the current State and a sub header which will display responses to commands. At any given point the user can type "help" in the text field and see a list of available commands for the current state. ![iOS Simulator Screen Shot Dec 2, 2014, 7.16.48 PM.png](https://bitbucket.org/repo/Apbnby/images/2694658107-iOS%20Simulator%20Screen%20Shot%20Dec%202,%202014,%207.16.48%20PM.png)

The starting state is at home and the only thing that the user can do at this point is type the "leave" (home) command and begin their quest. ![iOS Simulator Screen Shot Dec 2, 2014, 7.20.02 PM.png](https://bitbucket.org/repo/Apbnby/images/1218727089-iOS%20Simulator%20Screen%20Shot%20Dec%202,%202014,%207.20.02%20PM.png)

This is where the game gets most exciting. Users can enter battles by typing "battle" and they will be sent to the battle state and matched up with 1 - 5 enemies of different strengths. ![iOS Simulator Screen Shot Dec 2, 2014, 7.31.21 PM.png](https://bitbucket.org/repo/Apbnby/images/2599487778-iOS%20Simulator%20Screen%20Shot%20Dec%202,%202014,%207.31.21%20PM.png)

Each time the user types "attack" there will be an attack queued up for each enemy. ![iOS Simulator Screen Shot Dec 2, 2014, 7.32.34 PM.png](https://bitbucket.org/repo/Apbnby/images/1196924273-iOS%20Simulator%20Screen%20Shot%20Dec%202,%202014,%207.32.34%20PM.png)

When the user feels they have enough attacks queued up they can "end" their turn and execute one attack per enemy. This will also allow each enemy to execute one attack on the user's character. ![iOS Simulator Screen Shot Dec 2, 2014, 7.33.43 PM.png](https://bitbucket.org/repo/Apbnby/images/1994006436-iOS%20Simulator%20Screen%20Shot%20Dec%202,%202014,%207.33.43%20PM.png)

At this point the user can queue more attacks and keep ending turns and if they are lucky they will kill their enemies before their character dies. ![iOS Simulator Screen Shot Dec 2, 2014, 7.36.27 PM.png](https://bitbucket.org/repo/Apbnby/images/538728939-iOS%20Simulator%20Screen%20Shot%20Dec%202,%202014,%207.36.27%20PM.png)

When a character levels up (by getting 100 experience points) the user's character gets an extra 10 points added to their life capacity and their life is refilled for the next battle. ![iOS Simulator Screen Shot Dec 2, 2014, 7.42.47 PM.png](https://bitbucket.org/repo/Apbnby/images/495513536-iOS%20Simulator%20Screen%20Shot%20Dec%202,%202014,%207.42.47%20PM.png)

When a user faces a tough battle and can't come up on top the user will be sent to the game over state. ![iOS Simulator Screen Shot Dec 2, 2014, 7.44.54 PM.png](https://bitbucket.org/repo/Apbnby/images/674854331-iOS%20Simulator%20Screen%20Shot%20Dec%202,%202014,%207.44.54%20PM.png)

That is a brief summary of how Final Tapestry works. At this point the user will need to hit the "quit" button in the lower right hand corner in order to be brought back to the opening menu.

### Design patterns used ###

* **Singleton**: There is a ```GameStateManager``` singleton that is used in both the starting view where the user chooses their character and in the game view. The ```GameStateManager``` is a mechanism for the starting settings to persist to the actual game and handles the transitions to new states with a little help from the ```CommandLibrary``` class.
* **Command**: The command pattern is only used in battles. When the user queues an attack on his enemies the command pattern is being used. Each individual enemy (which is also just a sub class of the ```GameCharacter``` class) hangs on to the queued attacks and waits for the time that it needs to invoke one of the attacks on itself. The users character also does the same thing except that the attacks are never queued up.
* **Strategy**: The strategy pattern in this app is closely tied to the attack commands. The each ```Attack``` instance has a common interface to execute attacks but can use any different weapon in order to allow for many different types of attacks. The weapon is the strategy in this case 
* **Factory**: I used the factory pattern twice in this app. First there is a ```GetBattle``` static factory method on the ```BattleState``` this is used by the ```GameStateManager``` in order to know if a battle has ended or not and if the battle has ended it this factory method will return a fresh battle state or if the battle isn't over yet it will return the existing battle state. This allows battles to continue for an indefinite amount of time. Another static factory method is used on the ```Enemy``` class. The ```GetRandomEnemy``` factory method allows the random creation of new enemies to be used in new battle states.

### Patterns not used ###
* **Actor model**: The actor model pattern helps with concurrency. Objects used in the code are "actors" and can send messages to other actors if they have the "address" of the other actor. This allows for easy message sending between objects and makes concurrent programming easier to handle from the developer's perspective.
* **Composite pattern**: An object has a member which is itself. This allows instances of the object to be composed in a tree-like structure which gives a lot more flexibility and functionality to any instance of the object. The interface (API) of the object is the same for any instance of the object but different objects can actually have a lot of different abilities because the different members that compose the object can essentially all work together.

### My experience with the final project ###
Learning Swift was a blast. The language itself makes a lot of sense and isn't too hard to pick up. Swift is a strongly typed compiled language that doesn't require you to specify the type explicitly for every variable. This leads to a clean easy to read syntax that isn't too verbose.

The apple testing framework XCTest was fun to get to know. It made writing unit tests and functional tests easy. I found writing functional tests to be especially helpful. I was able to load the view controller class for my various views and actually simulate the user pressing buttons and typing things in the input fields and then check that everything going on under the hood had the correct values.

This project was also a great chance to review and implement some of the design patterns that we studied during the semester. I can definitely see how these will be useful in real world projects.

### My experience in the class ###
I thought this class was well taught and provided a lot of great information. I feel that the concepts taught were applicable in many different development projects. I liked the high energy and class participation of the instructor. All in all a great experience.

### My thoughts on object oriented development ###
Object oriented principles --for the most part-- aren't too hard to wrap one's head around. They provide a great way to structure a project and separate concerns between objects. Things can still get complex and messy at times but if the developer is careful an object oriented approach can work quite well for most projects. Definitely good concepts to understand for any developer.