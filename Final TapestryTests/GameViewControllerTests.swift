//
//  GameViewControllerTests.swift
//  Final Tapestry
//
//  Created by Kevin Mann on 11/29/14.
//  Copyright (c) 2014 Freedom Software. All rights reserved.
//

import UIKit
import XCTest

class GameViewControllerTests: XCTestCase {

    var gc : GameViewController!

    override func setUp() {
        super.setUp()
        var storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: NSBundle(forClass: self.dynamicType))
        self.gc = storyBoard.instantiateViewControllerWithIdentifier("GameVC") as GameViewController
        self.gc.loadView()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testUserStartsAtHome() {
        self.gc.viewDidLoad()
        XCTAssertEqual("home",
            self.gc.gameStateManager.getCurrentState().name,
            "Test if when the view loads the user is always at home")
    }

    func testMainStatusLabel() {
        var newPlayer = Elf(name: "Bob")
        self.gc.gameStateManager.setCurrentCharacter(newPlayer)
        self.gc.viewDidLoad()
        XCTAssertEqual("Welcome, Bob!\nThe Quest Begins!",
            self.gc.mainStatusLabel.text!,
            "Test that when the game starts the users name is mentioned")
    }
    
    func testIncorrectCommand() {
        self.gc.viewDidLoad()
        self.gc.commandTextField.text = "battle"
        self.gc.enteredCommand(self.gc)
        // Check response listing available commands for home state
        var targetHomeResponse = "Sorry, invalid command, you can only do these commands right now:\n\nstats\nhelp\nleave\n"
        XCTAssertEqual(targetHomeResponse,
            self.gc.commandResponseLabel.text!,
            "Test an invalid command leads to an error message with the available commands listed")
    }
    
    func testLeaveHomeState() {
        // View loads: user is at home
        self.gc.viewDidLoad()
        // User enters the "leave" command and hits enter
        self.gc.commandTextField.text = "leave"
        self.gc.enteredCommand(self.gc)
        // Check if current state is now questing
        XCTAssertEqual("quest",
            self.gc.gameStateManager.getCurrentState().name,
            "Test that typing the leave command leads to the quest state")
    }
    
    func testEnterBattleState() {
        self.gc.viewDidLoad()
        // We have to be in a quest state to be able to enter the battle state
        self.gc.gameStateManager.setCurrentState(QuestState())
        // User types "battle" and hits enter
        self.gc.commandTextField.text = "battle"
        self.gc.enteredCommand(self.gc)
        // Check if current state is now battle
        XCTAssertEqual("battle",
            self.gc.gameStateManager.getCurrentState().name,
            "Test that typing the battle command leads to the battle state")
    }
    
    func testLoseBattleToGameOverState() {
        self.gc.viewDidLoad()
        // Go to quest state
        self.gc.commandTextField.text = "leave"
        self.gc.enteredCommand(self.gc)
        // Go to battle state
        self.gc.commandTextField.text = "battle"
        self.gc.enteredCommand(self.gc)
        // Set life to one health point
        self.gc.gameStateManager.getCurrentCharacter().currentHealth = 1
        // End turn without attacking
        self.gc.commandTextField.text = "end"
        self.gc.enteredCommand(self.gc)
        // Check that we are now in the game over state
        XCTAssertEqual("game over",
            self.gc.gameStateManager.getCurrentState().name,
            "Test that typing the leave command leads to the quest state")
    }
    
    func testWinBattle() {
        self.gc.viewDidLoad()
        // Go to quest state
        self.gc.commandTextField.text = "leave"
        self.gc.enteredCommand(self.gc)
        // Go to battle state
        self.gc.commandTextField.text = "battle"
        self.gc.enteredCommand(self.gc)
        // Set life to one health point
        for enemy in self.gc.gameStateManager.getCurrentState().enemies {
            enemy.currentHealth = 1
        }
        // End turn without attacking
        self.gc.commandTextField.text = "attack"
        self.gc.enteredCommand(self.gc)
        self.gc.commandTextField.text = "end"
        self.gc.enteredCommand(self.gc)
        // Check that we are now in the game over state
        XCTAssertEqual("quest",
            self.gc.gameStateManager.getCurrentState().name,
            "Test that killing all enemies leads to the quest state")

    }

    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }

}
