//
//  Final_TapestryTests.swift
//  Final TapestryTests
//
//  Created by Kevin Mann on 11/24/14.
//  Copyright (c) 2014 Freedom Software. All rights reserved.
//

import UIKit
import XCTest

class StartViewControllerTests: XCTestCase {
    
    var vc : StartViewController!
    
    override func setUp() {
        super.setUp()
        var storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: NSBundle(forClass: self.dynamicType))
        self.vc = storyBoard.instantiateViewControllerWithIdentifier("StartVC") as StartViewController
        self.vc.loadView()
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testSubmitNameAsElf() {
        // User types "Butt" as their name
        self.vc.nameTextField.text = "Butt"
        // User taps the "Elf" button
        self.vc.choseElf(self.vc)
        let character = Elf(name: "Butt")
        // Tests that everything worked correctly after tapping the elf button
        XCTAssertEqual(self.vc.gameStateManager.getCurrentCharacter().name, character.name, "Test if submitting your name as an elf actually works")
    }
    
    func testSubmitNameAsDwarf() {
        // Test submitting the dwarf button
        self.vc.nameTextField.text = "Butt"
        self.vc.choseElf(self.vc)
        let character = Dwarf(name: "Butt")
        XCTAssertEqual(self.vc.gameStateManager.getCurrentCharacter().name, character.name, "Test if submitting your name as an dwarf actually works")
    }
    
    func testSubmitNameAsHuman() {
        // Test submitting the human button
        self.vc.nameTextField.text = "Butt"
        self.vc.choseElf(self.vc)
        let character = Dwarf(name: "Butt")
        XCTAssertEqual(self.vc.gameStateManager.getCurrentCharacter().name, character.name, "Test if submitting your name as an human actually works")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }
    
}
